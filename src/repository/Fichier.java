package repository;

/**
 * Objet Composant
 * @author ANDRIAMANGA Vahiniaina
 * @author HEQUET Jonathan
 * @author FEJZOVSKI Doruntine
 */
public abstract class Fichier {

	public String name;

	/**
	 * Constructeur
	 * 
	 * @param string name
	 */
	public Fichier(String name) {

		this.name = name;
	};

	/**
	 * Methode abstraite pour r�cup�rer la taille d'un fichier
	 */
	public abstract int getSize();
	
	
	public abstract boolean SearchSon(Fichier racine, Fichier f);
}
