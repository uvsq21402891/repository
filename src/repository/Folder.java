
package repository;

/**
 * Objet Composite contentant une liste de Fichiers
 * @author ANDRIAMANGA Vahiniaina
 * @author HEQUET Jonathan
 * @author FEJZOVSKI Doruntine
 */

import java.util.ArrayList;

public class Folder extends Fichier {

	public ArrayList<Fichier> contain;

	/**
	 * Folder constructeur Liste de Fichiers
	 * 
	 * @param name
	 */
	public Folder(String name) {
		super(name);
		contain = new ArrayList<Fichier>();
	}

	/**
	 * Ajout d'un folder
	 * 
	 * @param un
	 *            Fichier n
	 */

	public boolean addFichier(Folder f) {

		if (f.SearchSon(f, this)) {
			contain.add(f);
			System.out.println("good" + " " + f.toString() + " " + this.toString());
			return true;
		}
		System.out.println("bad" + " " + f.toString() + " " + this.toString());
		return false;

	}

	/**
	 * Ajout d'un SimpleFichier par surcharge de m�thode
	 * 
	 * @param un
	 *            SimpleFichier n
	 */
	public boolean addFichier(SimpleFichier n) {
		contain.add(n);
		return true;
	}

	/**
	 * Calcule la taille d'un folder
	 * 
	 * @see repository.Fichier#getSize()
	 */
	@Override
	public int getSize() {
		int taille = 0;

		for (Fichier fichier : contain) {

			taille = taille + fichier.getSize();
		}

		return taille;

	}

	/**
	 * 
	 * @param Fichier
	 *            f
	 * @return true si les fichiers sont identiques false sinon
	 */
	private boolean isSameFolder(Fichier f) {

		if (this == f)
			return true;
		return false;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.name;
	}

	public boolean SearchSon(Fichier root, Fichier f) {

		boolean tmp = true;

		if (root == f)
			return false;

		for (Fichier fichier : contain) {

			if (fichier == f)
				return false;
			tmp &= fichier.SearchSon(fichier, f);
		}

		return tmp;
	}

}
