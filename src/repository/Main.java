package repository;

/**
 * @author ANDRIAMANGA Vahiniaina
 * @author HEQUET Jonathan
 * @author FEJZOVSKI Doruntine
 */

public class Main {

	public static void main(String[] args) {
		
		Folder racine = new Folder("Racine");
		Folder f1 = new Folder("F1");
		Folder f2 = new Folder("F2");
		Folder f3 = new Folder("F3");
		Folder f4 = new Folder("F4");
	
		SimpleFichier sf1 = new SimpleFichier("sf1", 12);
		SimpleFichier sf2 = new SimpleFichier("sf2", 10);
	
		racine.addFichier(f1); // on ajoute f1 dans racine
		
		racine.addFichier(sf1); // on ajoute sf1 dans racine
		
		f1.addFichier(sf2); // on ajoute sf2 dans f1
		f1.addFichier(f2);
		f2.addFichier(f3);
		f2.addFichier(f1);
		
		System.out.println(racine.getSize());
		
		
	}

}
