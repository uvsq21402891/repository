package repository;

/**
 * Objet Composite
 * @author ANDRIAMANGA Vahiniaina
 * @author HEQUET Jonathan
 * @author FEJZOVSKI Doruntine
 *
 */
public class SimpleFichier extends Fichier {

	private int size;

	/**
	 * @param name
	 * @param size
	 */
	public SimpleFichier(String name, int size) {

		super(name);
		this.setSize(size);
	}

	/**
	 * Renvoie la taille d'un fichier (polymorphisme)
	 * 
	 * @see repository.Fichier#getSize()
	 * @return size
	 */
	public int getSize() {
		return size;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return size + name;
	}

	/**
	 * Setter size : modifie la taille d'un fichier
	 * 
	 * @param size
	 */
	public void setSize(int size) {
		this.size = size;
	}

	
	@Override
	public boolean SearchSon(Fichier root, Fichier f) {
		
		return true;
	}

}
